package imsg.com.trainingapplication.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import imsg.com.trainingapplication.R;
import imsg.com.trainingapplication.model.Activity;

/**
 * Created by henry on 10/18/2017.
 */

public class ListActivitiesAdapter extends ArrayAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Activity> activities;

    public ListActivitiesAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<Activity> objects) {
        super(context, resource, objects);

        this.context = context;
        this.activities = objects;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_list_view, parent, false);
            viewHolder.txtEndTime = (TextView) convertView.findViewById(R.id.txtEndTime);
            viewHolder.txtStartTime = (TextView) convertView.findViewById(R.id.txtStartTime);
            viewHolder.txtActivityContent = (TextView) convertView.findViewById(R.id.txtActivityContent);
            viewHolder.txtActivityTitle = (TextView) convertView.findViewById(R.id.txtActivityTitle);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Activity activity = activities.get(position);
        if (activity != null) {
            viewHolder.txtStartTime.setText(activity.getStartTime());
            viewHolder.txtEndTime.setText(activity.getEndTime());
            viewHolder.txtActivityContent.setText(activity.getContent());
            viewHolder.txtActivityTitle.setText(activity.getTitle());

        }

        return convertView;
    }

    public class ViewHolder {
        public TextView txtActivityTitle;
        public TextView txtActivityContent;
        public TextView txtStartTime;
        public TextView txtEndTime;
    }
}
