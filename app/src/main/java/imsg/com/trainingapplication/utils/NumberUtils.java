package imsg.com.trainingapplication.utils;

/**
 * Created by henry on 6/30/2017.
 */

public class NumberUtils {

    public static String increasingNumber(String s){
        return String.valueOf(Integer.parseInt(s)+1);
    }
    public static String decreasingNumber(String s){
        return String.valueOf(Integer.parseInt(s)-1);
    }

    public static String price(int s){
        StringBuilder pri=new StringBuilder(String.valueOf(s));
        if ((int)s/1000000>=1){
            pri.insert(pri.length()-6,".");
            pri.insert(pri.length()-3,".");
            return pri.toString() +" vnđ";
        }
        if ((int)s/1000>=1){
            pri.insert(pri.length()-3,".");
            return pri.toString() +" vnđ";
        }
        return pri.toString() +" vnđ";

    }


}
