package imsg.com.trainingapplication.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mj_eilers on 15-02-18.
 */
public class DateUtil {

    public static Calendar convertToDate(String date)
    {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertedDate = new Date();
            try {
                convertedDate = sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(convertedDate);

            return calendar;
        }
        catch (Exception e) {
//            e.printStackTrace();
            return null;
        }

    }

    public static String getTime(Calendar calendar){
        String time=new SimpleDateFormat("HH:mm").format(calendar.getTime());
        return time;
    }

    public static String convertToString(long intdate)
    {
        Date mDate = new Date(intdate*1000);
        Calendar date=Calendar.getInstance();
        date.setTime(mDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return sdf.format(date.getTime());

    }


}
