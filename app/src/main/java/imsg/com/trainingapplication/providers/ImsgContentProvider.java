package imsg.com.trainingapplication.providers;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import imsg.com.trainingapplication.database.DatabaseHelper;
import imsg.com.trainingapplication.database.TbEvents;
import imsg.com.trainingapplication.database.TbMetaPost;
import imsg.com.trainingapplication.database.TbPosts;
import imsg.com.trainingapplication.database.TbTerms;


/**
 * Created by hinh1 on 7/21/2017.
 */

public class ImsgContentProvider extends ContentProvider {
    private static final String TAG = ImsgContentProvider.class.getSimpleName();

    public static final String SCHEME = "content";
    public static final String AUTHORITY = "imsg.com.trainingapplication.providers.ImsgContentProvider";

    public static final class URIS {

        public static final Uri Post_URI = Uri.parse(SCHEME + "://" + AUTHORITY + "/" + Paths.Post);
        public static final Uri MetaPost_URI = Uri.parse(SCHEME + "://" + AUTHORITY + "/" + Paths.MetaPost);
        public static final Uri Terms_URI = Uri.parse(SCHEME + "://" + AUTHORITY + "/" + Paths.Terms);
        public static final Uri Events_URI = Uri.parse(SCHEME + "://" + AUTHORITY + "/" + Paths.Events);

    }

    public static final class Paths {
        public static final String Post = "Post";
        public static final String MetaPost = "MetaPost";
        public static final String Terms = "Terms";
        public static final String Events = "Events";

    }

    private static final int Post_DIR = 0;
    private static final int Post_ID = 1;
    private static final int MetaPost_DIR = 2;
    private static final int MetaPost_ID = 3;
    private static final int Terms_DIR = 4;
    private static final int Terms_ID = 5;
    private static final int Events_DIR = 6;
    private static final int Events_ID = 7;


    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, Paths.Post, Post_DIR);
        sURIMatcher.addURI(AUTHORITY, Paths.Post + "/#", Post_ID);
        sURIMatcher.addURI(AUTHORITY, Paths.MetaPost, MetaPost_DIR);
        sURIMatcher.addURI(AUTHORITY, Paths.MetaPost + "/#", MetaPost_ID);
        sURIMatcher.addURI(AUTHORITY, Paths.Terms, Terms_DIR);
        sURIMatcher.addURI(AUTHORITY, Paths.Terms + "/#", Terms_ID);
        sURIMatcher.addURI(AUTHORITY, Paths.Events, Events_DIR);
        sURIMatcher.addURI(AUTHORITY, Paths.Events + "/#", Events_ID);
    }


    // database
    private DatabaseHelper database;



    @Override
    public boolean onCreate() {
        database = DatabaseHelper.getInstance(getContext());
        return false;
    }


    @Override
    public String getType(Uri uri) {

        switch (sURIMatcher.match(uri)) {
            case Post_ID:
                return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + Paths.Post;
            case Post_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + Paths.Post;

            case MetaPost_ID:
                return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + Paths.MetaPost;
            case MetaPost_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + Paths.MetaPost;

            case Terms_ID:
                return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + Paths.Terms;
            case Terms_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + Paths.Terms;

            case Events_ID:
                return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + Paths.Events;
            case Events_DIR:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + Paths.Events;


            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        // Uisng SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        String groupBy = null;
        String having = null;

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case Post_ID:
                queryBuilder.appendWhere(TbPosts.ID + "="
                        + uri.getLastPathSegment());
            case Post_DIR:
                queryBuilder.setTables(TbPosts.TABLE_NAME);
                break;

            case MetaPost_ID:
                queryBuilder.appendWhere(TbMetaPost.ID + "="
                        + uri.getLastPathSegment());
            case MetaPost_DIR:
                queryBuilder.setTables(TbMetaPost.TABLE_NAME);

            case Terms_ID:
                queryBuilder.appendWhere(TbTerms.ID + "="
                        + uri.getLastPathSegment());
            case Terms_DIR:
                queryBuilder.setTables(TbTerms.TABLE_NAME);
                break;

            case Events_ID:
                queryBuilder.appendWhere(TbEvents.ID + "="
                        + uri.getLastPathSegment());
            case Events_DIR:
                queryBuilder.setTables(TbEvents.TABLE_NAME);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, groupBy, having, sortOrder);

        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase dbConnection = database.getWritableDatabase();

        try {
            dbConnection.beginTransaction();

            switch (uriType) {
                case Post_DIR:
                case Post_ID:
                    final long post_Id = dbConnection.insertOrThrow(
                            TbPosts.TABLE_NAME, null, values);
                    final Uri newPost = ContentUris.withAppendedId(
                            URIS.Post_URI, post_Id);
                    dbConnection.setTransactionSuccessful();
                    return newPost;

                case MetaPost_DIR:
                case MetaPost_ID:
                    final long meta_Id = dbConnection.insertOrThrow(
                            TbMetaPost.TABLE_NAME, null, values);
                    final Uri newMeta = ContentUris.withAppendedId(
                            URIS.MetaPost_URI, meta_Id);
                    dbConnection.setTransactionSuccessful();
                    return newMeta;

                case Terms_DIR:
                case Terms_ID:
                    final long term_Id = dbConnection.insertOrThrow(
                            TbTerms.TABLE_NAME, null, values);
                    final Uri new_term_Id= ContentUris.withAppendedId(
                            URIS.Terms_URI, term_Id);
                    dbConnection.setTransactionSuccessful();
                    return new_term_Id;

                case Events_DIR:
                case Events_ID:
                    final long Events_Id = dbConnection.insertOrThrow(
                            TbEvents.TABLE_NAME, null, values);
                    final Uri new_Events_Id= ContentUris.withAppendedId(
                            URIS.Events_URI, Events_Id);
                    dbConnection.setTransactionSuccessful();
                    return new_Events_Id;

                default:
                    throw new IllegalArgumentException("Unknown URI: " + uri);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Insert Exception", e);
        } finally {
            dbConnection.endTransaction();
        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);

        final SQLiteDatabase dbConnection = database.getWritableDatabase();
        int deleteCount = 0;

        try {
            dbConnection.beginTransaction();

            switch (uriType) {
                case Post_DIR :
                    deleteCount = dbConnection.delete(TbPosts.TABLE_NAME,
                            selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                case Post_ID :
                    deleteCount = dbConnection.delete(TbPosts.TABLE_NAME,
                            TbPosts.ID + "=?", new String[]{uri
                                    .getPathSegments().get(1)});
                    dbConnection.setTransactionSuccessful();
                    break;

                case MetaPost_DIR :
                    deleteCount = dbConnection.delete(TbMetaPost.TABLE_NAME,
                            selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                case MetaPost_ID :
                    deleteCount = dbConnection.delete(TbMetaPost.TABLE_NAME,
                            TbMetaPost.ID+ "=?", new String[]{uri
                                    .getPathSegments().get(1)});
                    dbConnection.setTransactionSuccessful();
                    break;

                case Terms_DIR :
                    deleteCount = dbConnection.delete(TbTerms.TABLE_NAME,
                            selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                case Terms_ID :
                    deleteCount = dbConnection.delete(TbTerms.TABLE_NAME,
                            TbTerms.ID+ "=?", new String[]{uri
                                    .getPathSegments().get(1)});
                    dbConnection.setTransactionSuccessful();
                    break;

                case Events_DIR :
                    deleteCount = dbConnection.delete(TbEvents.TABLE_NAME,
                            selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                case Events_ID :
                    deleteCount = dbConnection.delete(TbEvents.TABLE_NAME,
                            TbEvents.ID+ "=?", new String[]{uri
                                    .getPathSegments().get(1)});
                    dbConnection.setTransactionSuccessful();
                    break;


                default :
                    throw new IllegalArgumentException("Unsupported URI:" + uri);
            }
        } finally {
            dbConnection.endTransaction();
        }


        // This is bad if you're batching deletions
//        if (deleteCount > 0) {
//            getContext().getContentResolver().notifyChange(uri, null);
//        }
        return deleteCount;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        final SQLiteDatabase dbConnection = database.getWritableDatabase();
        int updateCount = 0;

        try {
            dbConnection.beginTransaction();

            switch (uriType) {

                case Post_DIR :
                    updateCount = dbConnection.update(TbPosts.TABLE_NAME,
                            values, selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                case Post_ID :
                    final Long Id = ContentUris.parseId(uri);
                    updateCount = dbConnection.update(
                            TbPosts.TABLE_NAME,
                            values,
                            TbPosts.ID
                                    + "="
                                    + Id
                                    + (TextUtils.isEmpty(selection)
                                    ? ""
                                    : " AND (" + selection + ")"),
                            selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;

                case MetaPost_DIR :
                    updateCount = dbConnection.update(TbMetaPost.TABLE_NAME,
                            values, selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                case MetaPost_ID :
                    final Long metaId = ContentUris.parseId(uri);
                    updateCount = dbConnection.update(
                            TbMetaPost.TABLE_NAME,
                            values,
                            TbMetaPost.ID
                                    + "="
                                    + metaId
                                    + (TextUtils.isEmpty(selection)
                                    ? ""
                                    : " AND (" + selection + ")"),
                            selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;

                case Terms_DIR :
                    updateCount = dbConnection.update(TbTerms.TABLE_NAME,
                            values, selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                case Terms_ID :
                    final Long termId = ContentUris.parseId(uri);
                    updateCount = dbConnection.update(
                            TbTerms.TABLE_NAME,
                            values,
                            TbTerms.ID
                                    + "="
                                    + termId
                                    + (TextUtils.isEmpty(selection)
                                    ? ""
                                    : " AND (" + selection + ")"),
                            selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;

                case Events_DIR :
                    updateCount = dbConnection.update(TbTerms.TABLE_NAME,
                            values, selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                case Events_ID :
                    final Long eventsId = ContentUris.parseId(uri);
                    updateCount = dbConnection.update(
                            TbEvents.TABLE_NAME,
                            values,
                            TbTerms.ID
                                    + "="
                                    + eventsId
                                    + (TextUtils.isEmpty(selection)
                                    ? ""
                                    : " AND (" + selection + ")"),
                            selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;

                default :
                    throw new IllegalArgumentException("Unsupported URI:" + uri);
            }
        } finally {
            dbConnection.endTransaction();
        }

        // This is bad if you're batching updates
//        if (updateCount > 0) {
//            getContext().getContentResolver().notifyChange(uri, null);
//        }
        return updateCount;
    }


}
