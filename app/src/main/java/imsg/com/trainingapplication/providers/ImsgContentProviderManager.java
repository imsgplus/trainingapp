package imsg.com.trainingapplication.providers;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import imsg.com.trainingapplication.database.TbEvents;
import imsg.com.trainingapplication.database.TbMetaPost;
import imsg.com.trainingapplication.database.TbPosts;
import imsg.com.trainingapplication.model.Address;
import imsg.com.trainingapplication.model.Lesson;
import imsg.com.trainingapplication.sync.remote.RemoteEvent;
import imsg.com.trainingapplication.sync.remote.RemoteMetaPost;
import imsg.com.trainingapplication.sync.remote.RemotePosts;
import imsg.com.trainingapplication.utils.DateUtil;

/**
 * Created by hinh1 on 9/8/2017.
 */

public class ImsgContentProviderManager {
    public static ImsgContentProviderManager mInstance;
    private static ContentResolver resolver;
    public ImsgContentProviderManager(Context context) {
        resolver=context.getContentResolver();
    }

    public static ImsgContentProviderManager getInstance(Context ctx) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.install
        // See this article for more information: http://bit.ly/6LRzfx
        if (mInstance == null) {
            mInstance = new ImsgContentProviderManager(ctx.getApplicationContext());
        }
        return mInstance;
    }

    public ArrayList<Lesson> loadLesson(){
        RemotePosts remotePosts;
        ArrayList<Lesson> lessons=new ArrayList<>();
        Lesson lesson;
        RemoteEvent remoteEvent;


        final Cursor c = resolver.query(
                ImsgContentProvider.URIS.Events_URI,
                TbEvents.ALL_COLUMNS,
                TbEvents.IS_DELETED + "=?",
                new String[] {"0"},
                TbEvents.ID + " DESC"
        );

        for( c.moveToFirst(); !c.isAfterLast(); c.moveToNext() ) {

            remoteEvent = new RemoteEvent(c);
            lesson=new Lesson();
            lesson.setId(remoteEvent.getId());
            lesson.setTitle(loadPostById(remoteEvent.getEvent_id()).getPostTitle());
            lesson.setStart(DateUtil.convertToString(remoteEvent.getStart()));
            lesson.setEnd(DateUtil.convertToString(remoteEvent.getEnd()));
            lessons.add(lesson);




        }
        c.close();
        return lessons;
    }

    public Lesson loadLessonById(long postId){
        RemotePosts remotePosts;
        ArrayList<Lesson> lessons=new ArrayList<>();
        Lesson lesson=null;
        RemoteMetaPost metaPost1;
        RemoteMetaPost metaPost2;

        final Cursor c = resolver.query(
                ImsgContentProvider.URIS.Post_URI,
                TbPosts.ALL_COLUMNS,
                TbPosts.IS_DELETED + "=? and "+TbPosts.Post_Type+"=? and "+TbPosts.ID+"=?",
                new String[] {"0","tribe_events",String.valueOf(postId)},
                TbPosts.ID + " DESC"
        );
        if (c.getCount()>0){
            c.moveToFirst();
            remotePosts = new RemotePosts(c);
            lesson=new Lesson();
            lesson.setId(remotePosts.getId());
            lesson.setTitle(remotePosts.getPostTitle());
            //lesson.setUrl(remotePosts.getGuid());
            metaPost1=loadMetaPost(remotePosts.getId(),"_EventStartDate");
            metaPost2=loadMetaPost(remotePosts.getId(),"_EventEndDate");
            if (metaPost1!=null && metaPost2!=null){
                lesson.setStart(metaPost1.getValue());
                lesson.setEnd(metaPost2.getValue());
            }
            metaPost1=loadMetaPost(remotePosts.getId(),"_EventVenueID");
            if (metaPost1!=null){
                lesson.setVenueId(Long.valueOf(metaPost1.getValue()));
            }
        }
        c.close();
        return lesson;
    }
    public Address loadAddressById(long postId){
        Address address=null;
        RemotePosts remotePosts;
        RemoteMetaPost metaPost1;
        RemoteMetaPost metaPost2;

        final Cursor c = resolver.query(
                ImsgContentProvider.URIS.Post_URI,
                TbPosts.ALL_COLUMNS,
                TbPosts.IS_DELETED + "=? and "+TbPosts.Post_Type+"=? and "+TbPosts.Post_ID+"=?",
                new String[] {"0","tribe_venue",String.valueOf(postId)},
                TbPosts.ID + " DESC"
        );
        if (c.getCount()>0){
            c.moveToFirst();
            address=new Address();
            remotePosts = new RemotePosts(c);
            //address.setId(remotePosts.getId());
            //address.setAddress(remotePosts.getPostTitle());
            metaPost1=loadMetaPost(remotePosts.getId(),"_VenueAddress");
            if (metaPost1!=null){
                address.setAddress(metaPost1.getValue());
            }
            metaPost2=loadMetaPost(remotePosts.getId(),"_VenueCountry");
            if (metaPost2!=null){
                address.setCountry(metaPost2.getValue());
            }
            metaPost1=loadMetaPost(remotePosts.getId(),"_VenueCity");
            if (metaPost1!=null){
                address.setCity(metaPost1.getValue());
            }
        }
        c.close();
        return address;
    }







    public RemoteMetaPost loadMetaPost(long postId,String key) {
        final Cursor c = resolver.query(
                ImsgContentProvider.URIS.MetaPost_URI,
                TbMetaPost.ALL_COLUMNS,
                TbMetaPost.IS_DELETED + "=? and "+TbMetaPost.PostId+"=? and "+TbMetaPost.Key+"=?",
                new String[] {"0",String.valueOf(postId),key},
                null
        );
        RemoteMetaPost metaPost;

        if (c.getCount()>0){
            c.moveToFirst();
            metaPost=new RemoteMetaPost(c);
            return metaPost;
        }
        c.close();
        return null;
    }

    public RemotePosts loadPostById(long postId){
        RemotePosts remotePosts=null;

        final Cursor c = resolver.query(
                ImsgContentProvider.URIS.Post_URI,
                TbPosts.ALL_COLUMNS,
                TbPosts.IS_DELETED + "=? and "+TbPosts.Post_ID+"=?",
                new String[] {"0",String.valueOf(postId)},
                TbPosts.ID + " DESC"
        );
        if (c.getCount()>0){
            c.moveToFirst();
            remotePosts = new RemotePosts(c);
        }
        c.close();
        return remotePosts;
    }




}
