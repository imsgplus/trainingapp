package imsg.com.trainingapplication.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by hinh1 on 7/20/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "IMSGDB.db";
    private static final int DATABASE_VERSION = 1;

    private static DatabaseHelper mInstance = null;

    public static DatabaseHelper getInstance(Context ctx) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.install
        // See this article for more information: http://bit.ly/6LRzfx
        if (mInstance == null) {
            mInstance = new DatabaseHelper(ctx.getApplicationContext());
        }
        return mInstance;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        TbPosts.onCreate(database);
        TbMetaPost.onCreate(database);
        TbTerms.onCreate(database);
        TbEvents.onCreate(database);
        Log.d("fuck","aaaaaaaaaa");

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion) {

        TbPosts.onUpgrade(database, oldVersion, newVersion);
        TbMetaPost.onUpgrade(database, oldVersion, newVersion);
        TbTerms.onUpgrade(database, oldVersion, newVersion);
        TbEvents.onUpgrade(database, oldVersion, newVersion);
    }

}

