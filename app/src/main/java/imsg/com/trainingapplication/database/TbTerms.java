package imsg.com.trainingapplication.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by hinh1 on 9/7/2017.
 */

public class TbTerms {
    public static final String TABLE_NAME = "Terms";

    public static final String ID = TABLE_NAME + "_" + "Id";
    public static final String Term_ID = TABLE_NAME + "_" + "Term_ID";
    public static final String Name = TABLE_NAME + "_" + "Term_Name";
    public static final String Slug = TABLE_NAME + "_" + "Slug";
    public static final String CreateDate = TABLE_NAME + "_" + "CreateDate";
    public static final String UpdateDate = TABLE_NAME + "_" + "UpdateDate";
    public static final String SYNC_STATUS = TABLE_NAME + "_" + "SyncStatus";
    public static final String IS_DELETED = TABLE_NAME + "_" + "isDeleted";


    public static String[] ALL_COLUMNS = new String[]{
            ID,
            Term_ID,
            Name,
            Slug,
            CreateDate,
            UpdateDate,
            SYNC_STATUS,
            IS_DELETED
    };

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table IF NOT EXISTS "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement not null, "
            +Term_ID +" integer,"
            +Name + " text,"
            +Slug + " text,"
            + CreateDate + " text,"
            + UpdateDate + " text,"
            + SYNC_STATUS + " integer,"
            + IS_DELETED + " integer"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }
    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TbTerms.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
