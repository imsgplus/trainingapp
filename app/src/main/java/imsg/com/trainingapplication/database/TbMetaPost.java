package imsg.com.trainingapplication.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by hinh1 on 9/8/2017.
 */

public class TbMetaPost {
    public static final String TABLE_NAME = "MetaPost";

    public static final String ID = TABLE_NAME + "_" + "Id";

    public static final String PostId = TABLE_NAME + "_" + "PostId";
    public static final String MetaPost_ID = TABLE_NAME + "_" + "MetaPost_ID";
    public static final String Post_Id = TABLE_NAME + "_" + "Post_Id";
    public static final String Key = TABLE_NAME + "_" + "Key";
    public static final String Value = TABLE_NAME + "_" + "Value";

    public static final String CreateDate = TABLE_NAME + "_" + "CreateDate";
    public static final String UpdateDate = TABLE_NAME + "_" + "UpdateDate";
    public static final String SYNC_STATUS = TABLE_NAME + "_" + "SyncStatus";
    public static final String IS_DELETED = TABLE_NAME + "_" + "isDeleted";

    public static String[] ALL_COLUMNS = new String[]{
            ID,
            PostId,
            MetaPost_ID,
            Post_Id,
            Key,
            Value,
            CreateDate,
            UpdateDate,
            SYNC_STATUS,
            IS_DELETED
    };

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table IF NOT EXISTS "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement not null, "
            + PostId +" integer,"
            + MetaPost_ID +" integer,"
            + Post_Id +" integer,"
            + Key + " text,"
            + Value + " text,"
            + CreateDate + " text,"
            + UpdateDate + " text,"
            + SYNC_STATUS + " integer,"
            + IS_DELETED + " integer"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }
    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TbMetaPost.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
