package imsg.com.trainingapplication.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by hinh1 on 9/8/2017.
 */

public class TbPosts {
    public static final String TABLE_NAME = "Posts";

    public static final String ID = TABLE_NAME + "_" + "Id";
    public static final String Post_ID = TABLE_NAME + "_" + "Post_ID";
    public static final String Post_Author = TABLE_NAME + "_" + "Post_Author";
    public static final String Post_Date = TABLE_NAME + "_" + "Post_Date";
    public static final String Post_Content = TABLE_NAME + "_" + "Post_Content";
    public static final String Post_Title = TABLE_NAME + "_" + "Post_Title";
    public static final String Post_Name = TABLE_NAME + "_" + "Post_Name";
    public static final String Guid = TABLE_NAME + "_" + "Guid";
    public static final String Post_Type = TABLE_NAME + "_" + "Post_Type";
    public static final String CreateDate = TABLE_NAME + "_" + "CreateDate";
    public static final String UpdateDate = TABLE_NAME + "_" + "UpdateDate";
    public static final String SYNC_STATUS = TABLE_NAME + "_" + "SyncStatus";
    public static final String IS_DELETED = TABLE_NAME + "_" + "isDeleted";

    public static String[] ALL_COLUMNS = new String[]{
            ID,
            Post_ID,
            Post_Author,
            Post_Date,
            Post_Content,
            Post_Title,
            Post_Name,
            Guid,
            Post_Type,
            CreateDate,
            UpdateDate,
            SYNC_STATUS,
            IS_DELETED
    };

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table IF NOT EXISTS "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement not null, "
            + Post_ID +" integer,"
            + Post_Author + " integer,"
            + Post_Date + " text,"
            + Post_Content + " text,"
            + Post_Title + " text,"
            + Post_Name + " text,"
            + Guid + " text,"
            + Post_Type + " text,"
            + CreateDate + " text,"
            + UpdateDate + " text,"
            + SYNC_STATUS + " integer,"
            + IS_DELETED + " integer"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }
    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TbPosts.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
