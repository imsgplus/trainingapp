package imsg.com.trainingapplication.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class TbEvents {
    public static final String TABLE_NAME = "Events";

    public static final String ID = TABLE_NAME + "_" + "Id";
    public static final String Event_ID = TABLE_NAME + "_" + "Event_ID";
    public static final String Start = TABLE_NAME + "_" + "Start";
    public static final String End = TABLE_NAME + "_" + "End";
    public static final String CreateDate = TABLE_NAME + "_" + "CreateDate";
    public static final String UpdateDate = TABLE_NAME + "_" + "UpdateDate";
    public static final String SYNC_STATUS = TABLE_NAME + "_" + "SyncStatus";
    public static final String IS_DELETED = TABLE_NAME + "_" + "isDeleted";


    public static String[] ALL_COLUMNS = new String[]{
            ID,
            Event_ID,
            Start,
            End,
            CreateDate,
            UpdateDate,
            SYNC_STATUS,
            IS_DELETED
    };

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table IF NOT EXISTS "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement not null, "
            +Event_ID +" integer,"
            +Start + " integer,"
            +End + " integer,"
            + CreateDate + " text,"
            + UpdateDate + " text,"
            + SYNC_STATUS + " integer,"
            + IS_DELETED + " integer"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }
    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TbEvents.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
