package imsg.com.trainingapplication;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import imsg.com.trainingapplication.providers.ImsgContentProvider;
import imsg.com.trainingapplication.service.SyncService;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        loadData();
        Intent intent=new Intent(this, SyncService.class);
        intent.setAction(Intent.ACTION_SYNC);
        startService(intent);
        loadData2();
    }

    public void loadData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        startActivity(new Intent(MainActivity.this, EnterCodeActivity.class));
    }

    private void loadData2(){
        //LOAD DATA
        Cursor cursor = getContentResolver().query(ImsgContentProvider.URIS.Events_URI, null, null, null, null);
        Toast.makeText(this,String.valueOf(cursor.getCount()),Toast.LENGTH_LONG).show();
        cursor.close();

    }
}
