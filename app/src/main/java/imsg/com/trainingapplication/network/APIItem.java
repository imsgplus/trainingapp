package imsg.com.trainingapplication.network;

import java.io.IOException;

import imsg.com.trainingapplication.app.Application;
import imsg.com.trainingapplication.model.response.EventsResponse;
import imsg.com.trainingapplication.model.response.MetaPostResponse;
import imsg.com.trainingapplication.model.response.PostResponse;
import imsg.com.trainingapplication.model.response.TermsResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Dung on 7/13/2017.
 */

public class APIItem {
    public static PostResponse getAllPost(){


        try{
            Call<PostResponse> call = Application.APISever.getAllPost();

            PostResponse data = null;
            Response<PostResponse> response = call.execute();
            if(response != null){
                data = response.body();
            }
            return data;
        }catch (IOException e){
            e.printStackTrace();

        }
        return null;
    }
    public static MetaPostResponse getAllMetaPost(){
        try{
            Call<MetaPostResponse> call = Application.APISever.getAllMetaPost();
            MetaPostResponse data = null;
            Response<MetaPostResponse> response = call.execute();
            if(response != null){
                data = response.body();
            }
            return data;
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public static TermsResponse getAllTerm(){
        try{
            Call<TermsResponse> call = Application.APISever.getAllTerms();
            TermsResponse data = null;
            Response<TermsResponse> response = call.execute();
            if(response != null){
                data = response.body();
            }
            return data;
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public static EventsResponse getAllEvents(){
        try{
            Call<EventsResponse> call = Application.APISever.getAllEvents();
            EventsResponse data = null;
            Response<EventsResponse> response = call.execute();
            if(response != null){
                data = response.body();
            }
            return data;
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

}
