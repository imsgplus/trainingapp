package imsg.com.trainingapplication.network;

import imsg.com.trainingapplication.model.response.EventsResponse;
import imsg.com.trainingapplication.model.response.MetaPostResponse;
import imsg.com.trainingapplication.model.response.PostResponse;
import imsg.com.trainingapplication.model.response.TermsResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Dung on 7/13/2017.
 */

public interface APIServer {

    //**********************************************************************************
    //post api
    @GET("api.php/wp5r_posts")
    Call<PostResponse> getAllPost();

    //**********************************************************************************
    // metapost api
    @GET("meta_api.php/wp_postmeta")
    Call<MetaPostResponse> getAllMetaPost();

    //**********************************************************************************
    // term api
    @GET("imsg_api.php/wp5r_terms")
    Call<TermsResponse> getAllTerms();

    //**********************************************************************************
    // event api
    @GET("imsg_event_api.php/wp5r_ai1ec_events")
    Call<EventsResponse> getAllEvents();


}
