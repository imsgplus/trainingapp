
package imsg.com.trainingapplication.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemoteMetaPost;

public class MetaPostResponse {

    @SerializedName("data")
    @Expose
    private List<RemoteMetaPost> data = null;

    public List<RemoteMetaPost> getData() {
        return data;
    }

    public void setData(List<RemoteMetaPost> data) {
        this.data = data;
    }

}
