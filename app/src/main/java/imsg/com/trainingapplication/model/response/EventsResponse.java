package imsg.com.trainingapplication.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemoteEvent;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class EventsResponse {
    @SerializedName("data")
    @Expose
    private List<RemoteEvent> data = null;

    public List<RemoteEvent> getData() {
        return data;
    }

    public void setData(List<RemoteEvent> data) {
        this.data = data;
    }
}
