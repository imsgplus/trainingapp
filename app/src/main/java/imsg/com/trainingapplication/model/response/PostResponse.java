
package imsg.com.trainingapplication.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemotePosts;


public class PostResponse {

    @SerializedName("data")
    @Expose
    private List<RemotePosts> data = null;

    public List<RemotePosts> getData() {
        return data;
    }

    public void setData(List<RemotePosts> data) {
        this.data = data;
    }

}
