package imsg.com.trainingapplication.model;

/**
 * Created by hinh1 on 9/8/2017.
 */

public class Lesson {
    private long id;
    private String start;
    private String end;
    private String title;
    private long venueId;
    private String url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getVenueId() {
        return venueId;
    }

    public void setVenueId(long venueId) {
        this.venueId = venueId;
    }

    public String getUrl(){return url;}

    public void setUrl(String url){this.url = url;}
}
