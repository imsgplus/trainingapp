package imsg.com.trainingapplication.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemoteTerm;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class TermsResponse {
    @SerializedName("data")
    @Expose
    private List<RemoteTerm> data = null;

    public List<RemoteTerm> getData() {
        return data;
    }

    public void setData(List<RemoteTerm> data) {
        this.data = data;
    }
}
