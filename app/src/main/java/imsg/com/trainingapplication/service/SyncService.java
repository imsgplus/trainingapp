package imsg.com.trainingapplication.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.Nullable;

import java.util.List;

import imsg.com.trainingapplication.app.Application;
import imsg.com.trainingapplication.database.TbEvents;
import imsg.com.trainingapplication.database.TbPosts;
import imsg.com.trainingapplication.database.TbTerms;
import imsg.com.trainingapplication.network.APIItem;
import imsg.com.trainingapplication.providers.ImsgContentProvider;
import imsg.com.trainingapplication.sync.remote.RemoteEvent;
import imsg.com.trainingapplication.sync.remote.RemotePosts;
import imsg.com.trainingapplication.sync.remote.RemoteTerm;
import imsg.com.trainingapplication.sync.synchronizers.EventsSynchronizer;
import imsg.com.trainingapplication.sync.synchronizers.PostsSynchronizer;
import imsg.com.trainingapplication.sync.synchronizers.TermSynchronizer;
import imsg.com.trainingapplication.sync.synchronizers.preprocessors.EventsPreProcessor;
import imsg.com.trainingapplication.sync.synchronizers.preprocessors.PostPreProcessor;
import imsg.com.trainingapplication.sync.synchronizers.preprocessors.TermPreProcessor;
import imsg.com.trainingapplication.utils.SyncUtil;

/**
 * Created by hinh1 on 7/30/2017.
 */

public class SyncService extends IntentService {
    private static final String TAG = SyncService.class.getSimpleName();
    public SyncService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SYNC))
        {
            Context context = Application.getContext();


            try {


                List<RemoteTerm> remoteTerm =  APIItem.getAllTerm().getData();
                List<RemotePosts> remotePostses =  APIItem.getAllPost().getData();
                List<RemoteEvent> remoteEvents =APIItem.getAllEvents().getData();
                Cursor localterms = context.getContentResolver().query(
                        ImsgContentProvider.URIS.Terms_URI,
                        TbTerms.ALL_COLUMNS,
                        null,
                        null,
                        null
                );

                SyncUtil.synchronize(
                        context,
                        remoteTerm,
                        localterms,
                        RemoteTerm.IDENTIFIERS,
                        new TermSynchronizer(context, false),
                        new TermPreProcessor()
                );
                localterms.close();

                Cursor localposts = context.getContentResolver().query(
                        ImsgContentProvider.URIS.Post_URI,
                        TbPosts.ALL_COLUMNS,
                        null,
                        null,
                        null
                );

                SyncUtil.synchronize(
                        context,
                        remotePostses,
                        localposts,
                        RemotePosts.IDENTIFIERS,
                        new PostsSynchronizer(context, false),
                        new PostPreProcessor()
                );
                localposts.close();

                Cursor localevents = context.getContentResolver().query(
                        ImsgContentProvider.URIS.Events_URI,
                        TbEvents.ALL_COLUMNS,
                        null,
                        null,
                        null
                );

                SyncUtil.synchronize(
                        context,
                        remoteEvents,
                        localevents,
                        RemoteEvent.IDENTIFIERS,
                        new EventsSynchronizer(context, false),
                        new EventsPreProcessor()
                );
                localevents.close();


/*                List<RemotePosts> remotePostses =  APIItem.getAllPost().getData();
                List<RemoteMetaPost> remoteMetaPosts =  APIItem.getAllMetaPost().getData();
                // Sync Post
                Cursor localposts = context.getContentResolver().query(
                        ImsgContentProvider.URIS.Post_URI,
                        TbPosts.ALL_COLUMNS,
                        null,
                        null,
                        null
                );

                SyncUtil.synchronize(
                        context,
                        remotePostses,
                        localposts,
                        RemotePosts.IDENTIFIERS,
                        new PostsSynchronizer(context, false),
                        new PostPreProcessor()
                );
                localposts.close();

                //sync metapost

                Cursor cursor=null;
                for (int i=0;i<remoteMetaPosts.size();i++){
                    cursor= context.getContentResolver().query(
                            ImsgContentProvider.URIS.Post_URI,
                            TbPosts.ALL_COLUMNS,
                            TbPosts.Post_ID + "=?",
                            new String[]{String.valueOf(remoteMetaPosts.get(i).getPost_Id())},
                            null
                    );
                    if (cursor.getCount()>0){
                        cursor.moveToFirst();
                        remoteMetaPosts.get(i).setPostId(cursor.getLong(cursor.getColumnIndex(TbPosts.ID)));
                    }
                }
                cursor.close();

                Cursor localMeta = context.getContentResolver().query(
                        ImsgContentProvider.URIS.MetaPost_URI,
                        TbMetaPost.ALL_COLUMNS,
                        null,
                        null,
                        null
                );

                SyncUtil.synchronize(
                        context,
                        remoteMetaPosts,
                        localMeta,
                        RemoteMetaPost.IDENTIFIERS,
                        new MetaPostsynchronizer(context, false),
                        new MetaPostPreProcessor()
                );
                localposts.close();*/



            }
            catch (Exception e)
            {
                e.printStackTrace();
                return;
            }

        }
    }
}
