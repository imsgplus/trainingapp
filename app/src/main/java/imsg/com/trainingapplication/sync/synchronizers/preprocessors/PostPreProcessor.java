package imsg.com.trainingapplication.sync.synchronizers.preprocessors;

import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemoteObject;
import imsg.com.trainingapplication.sync.remote.RemotePosts;

/**
 * Created by hinh1 on 9/8/2017.
 */

public class PostPreProcessor extends BasePreProcessor<RemotePosts> {
    @Override
    public void preProcessRemoteRecords(List<RemotePosts> records) {
        for( RemotePosts posts : records ) {
            if( posts != null ) {
                posts.setIsDeleted(false);
                posts.setSyncStatus(RemoteObject.SyncStatus.NO_CHANGES);
            }
        }
    }
}
