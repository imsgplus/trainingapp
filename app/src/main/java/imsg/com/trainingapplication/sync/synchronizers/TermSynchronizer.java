package imsg.com.trainingapplication.sync.synchronizers;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import imsg.com.trainingapplication.app.Application;
import imsg.com.trainingapplication.database.DatabaseHelper;
import imsg.com.trainingapplication.database.TbTerms;
import imsg.com.trainingapplication.providers.ImsgContentProvider;
import imsg.com.trainingapplication.sync.remote.RemoteObject;
import imsg.com.trainingapplication.sync.remote.RemoteTerm;
import imsg.com.trainingapplication.utils.DateUtil;
import imsg.com.trainingapplication.utils.SyncUtil;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class TermSynchronizer extends BaseSynchronizer<RemoteTerm> {
    private static final String TAG = TermSynchronizer.class.getSimpleName();

    private boolean mAllowDeletion;
    public TermSynchronizer(Context context, boolean allowDeletion) {
        super(context);
        mAllowDeletion = allowDeletion;
    }
    @Override
    protected void performSynchronizationOperations(Context context, List<RemoteTerm> inserts, List<RemoteTerm> updates, List<Long> deletions) {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        if( inserts.size() > 0 ) {
            doInsertOptimised(inserts);
        }

        for (RemoteTerm w : updates) {
            SyncUtil.Selection selection = SyncUtil.buildSelection(w.getIdentifiers());
            ContentProviderOperation op = ContentProviderOperation
                    .newUpdate(ImsgContentProvider.URIS.Terms_URI)
                    .withSelection(selection.getQuery(), selection.getValues())
                    .withValues(getContentValuesForRemoteEntity(w)).build();

            operations.add(op);
        }

        if( mAllowDeletion ) {
            for (Long id : deletions) {
                ContentProviderOperation op = ContentProviderOperation
                        .newDelete(ImsgContentProvider.URIS.Terms_URI)
                        .withSelection(TbTerms.ID + " = ? AND (" + TbTerms.SYNC_STATUS + "=? OR " + TbTerms.IS_DELETED + "=?)",
                                new String[]{String.valueOf(id), RemoteObject.SyncStatus.NO_CHANGES.ordinal() + "", "1"})
                        .build();

                operations.add(op);
            }
        }


        try {
            if( inserts.size() > 0 || operations.size() > 0 ) {
                context.getContentResolver().applyBatch(ImsgContentProvider.AUTHORITY, operations);
                context.getContentResolver().notifyChange(ImsgContentProvider.URIS.Terms_URI, null);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean isRemoteEntityNewerThanLocal(RemoteTerm remote, Cursor c) {
        try {
            Calendar remoteUpdatedTime = DateUtil.convertToDate(remote.getUpdatedAt());
            Calendar localUpdatedTime = DateUtil.convertToDate(c.getString(c.getColumnIndex(TbTerms.UpdateDate)));

            if( remoteUpdatedTime == null || localUpdatedTime == null )
                return true;

            RemoteObject.SyncStatus localSyncStatus = RemoteObject.SyncStatus.getSyncStatusFromCode(c.getInt(c.getColumnIndex(TbTerms.SYNC_STATUS)));

            return (remoteUpdatedTime.getTimeInMillis() > localUpdatedTime.getTimeInMillis()) && !localSyncStatus.equals(RemoteObject.SyncStatus.QUEUED_TO_SYNC);


        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public static List<Long> doInsertOptimised(List<RemoteTerm> inserts) {

        if( inserts == null )
            return null;

        Context context = Application.getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        DatabaseUtils.InsertHelper inserter = new DatabaseUtils.InsertHelper(db, TbTerms.TABLE_NAME);
        List<Long> insertedIds = new ArrayList<>();
        RemoteTerm remoteTerm;

        db.beginTransaction();

        try {
            int len = inserts.size();
            for (int i = 0; i < len; i++) {

                remoteTerm = inserts.get(i);

                inserter.prepareForInsert();

                if( remoteTerm.getCreatedAt() != null )
                    inserter.bind(inserter.getColumnIndex(TbTerms.CreateDate), remoteTerm.getCreatedAt());

                if( remoteTerm.getUpdatedAt() != null )
                    inserter.bind(inserter.getColumnIndex(TbTerms.UpdateDate), remoteTerm.getUpdatedAt());

                if( remoteTerm.getSyncStatus() != null )
                    inserter.bind(inserter.getColumnIndex(TbTerms.SYNC_STATUS), remoteTerm.getSyncStatus().ordinal());

                if( remoteTerm.getIsDeleted() != null )
                    inserter.bind(inserter.getColumnIndex(TbTerms.IS_DELETED), remoteTerm.getIsDeleted());

                if( remoteTerm.getTerm_id() != null )
                    inserter.bind(inserter.getColumnIndex(TbTerms.Term_ID), remoteTerm.getTerm_id());

                if( remoteTerm.getTermName() != null )
                    inserter.bind(inserter.getColumnIndex(TbTerms.Name), remoteTerm.getTermName());

                if( remoteTerm.getSlug() != null )
                    inserter.bind(inserter.getColumnIndex(TbTerms.Slug), remoteTerm.getSlug());


                insertedIds.add(inserter.execute());
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            inserter.close();
        }
        return insertedIds;
    }
}
