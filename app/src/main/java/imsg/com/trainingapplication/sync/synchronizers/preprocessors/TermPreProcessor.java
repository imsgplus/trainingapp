package imsg.com.trainingapplication.sync.synchronizers.preprocessors;

import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemoteObject;
import imsg.com.trainingapplication.sync.remote.RemoteTerm;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class TermPreProcessor extends BasePreProcessor<RemoteTerm> {
    @Override
    public void preProcessRemoteRecords(List<RemoteTerm> records) {
        for( RemoteTerm posts : records ) {
            if( posts != null ) {
                posts.setIsDeleted(false);
                posts.setSyncStatus(RemoteObject.SyncStatus.NO_CHANGES);
            }
        }
    }
}
