
package imsg.com.trainingapplication.sync.remote;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;

import imsg.com.trainingapplication.database.TbMetaPost;

public class RemoteMetaPost extends RemoteObject{

    public static final String[] IDENTIFIERS = new String[] {
            TbMetaPost.MetaPost_ID
    };
    @SerializedName("id")
    @Expose
    private Long metaId;
    @SerializedName("post_id")
    @Expose
    private Long post_Id;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("value")
    @Expose
    private String value;
    private Long postId;

    public Long getMetaId() {
        return metaId;
    }

    public void setMetaId(Long metaId) {
        this.metaId = metaId;
    }

    public Long getPost_Id() {
        return post_Id;
    }

    public void setPost_Id(Long post_Id) {
        this.post_Id = post_Id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public RemoteMetaPost(final Cursor cursor) {

        setId(cursor.getLong(cursor.getColumnIndex(TbMetaPost.ID)));
        setCreatedAt(cursor.getString(cursor.getColumnIndex(TbMetaPost.CreateDate)));
        setUpdatedAt(cursor.getString(cursor.getColumnIndex(TbMetaPost.UpdateDate)));
        setSyncStatus(SyncStatus.getSyncStatusFromCode(cursor.getInt(cursor.getColumnIndex(TbMetaPost.SYNC_STATUS))));
        setIsDeleted(cursor.getInt(cursor.getColumnIndex(TbMetaPost.IS_DELETED)) == 1);

        setPostId(cursor.getLong(cursor.getColumnIndex(TbMetaPost.PostId)));
        setMetaId(cursor.getLong(cursor.getColumnIndex(TbMetaPost.MetaPost_ID)));
        setPost_Id(cursor.getLong(cursor.getColumnIndex(TbMetaPost.Post_Id)));
        setKey(cursor.getString(cursor.getColumnIndex(TbMetaPost.Key)));
        setValue(cursor.getString(cursor.getColumnIndex(TbMetaPost.Value)));

    }

    public RemoteMetaPost() {
    }

    @Override
    public LinkedHashMap<String, String> getIdentifiers() {

        LinkedHashMap<String, String> identifiers = super.getIdentifiers();
        identifiers.put(IDENTIFIERS[0], getMetaId()+"");

        return identifiers;
    }


    @Override
    public void populateContentValues(ContentValues values) {
        values.put(TbMetaPost.CreateDate, getCreatedAt());
        values.put(TbMetaPost.UpdateDate, getUpdatedAt());
        values.put(TbMetaPost.SYNC_STATUS, getSyncStatus() != null ? getSyncStatus().ordinal() : null);
        values.put(TbMetaPost.IS_DELETED, getIsDeleted());

        values.put(TbMetaPost.PostId, getPostId());
        values.put(TbMetaPost.MetaPost_ID, getMetaId());
        values.put(TbMetaPost.Post_Id, getPost_Id());
        values.put(TbMetaPost.Key, getKey());
        values.put(TbMetaPost.Value, getValue());

    }
}
