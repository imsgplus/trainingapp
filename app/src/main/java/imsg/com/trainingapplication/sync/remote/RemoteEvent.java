package imsg.com.trainingapplication.sync.remote;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;

import imsg.com.trainingapplication.database.TbEvents;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class RemoteEvent extends RemoteObject  {

    public static final String[] IDENTIFIERS = new String[] {
            TbEvents.Event_ID
    };


    @SerializedName("id")
    @Expose
    private Long event_id;
    @SerializedName("start")
    @Expose
    private Long start;
    @SerializedName("end")
    @Expose
    private Long end;
    @SerializedName("venue")
    @Expose
    private String venue;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("province")
    @Expose
    private String province;
    @SerializedName("contact_name")
    @Expose
    private String contactName;
    @SerializedName("contact_phone")
    @Expose
    private String contactPhone;
    @SerializedName("contact_email")
    @Expose
    private String contactEmail;
    @SerializedName("contact_url")
    @Expose
    private String contactUrl;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public Long getEvent_id() {
        return event_id;
    }

    public void setEvent_id(Long event_id) {
        this.event_id = event_id;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactUrl() {
        return contactUrl;
    }

    public void setContactUrl(String contactUrl) {
        this.contactUrl = contactUrl;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public RemoteEvent(final Cursor cursor) {

        setId(cursor.getLong(cursor.getColumnIndex(TbEvents.ID)));
        setCreatedAt(cursor.getString(cursor.getColumnIndex(TbEvents.CreateDate)));
        setUpdatedAt(cursor.getString(cursor.getColumnIndex(TbEvents.UpdateDate)));
        setSyncStatus(SyncStatus.getSyncStatusFromCode(cursor.getInt(cursor.getColumnIndex(TbEvents.SYNC_STATUS))));
        setIsDeleted(cursor.getInt(cursor.getColumnIndex(TbEvents.IS_DELETED)) == 1);

        setEvent_id(cursor.getLong(cursor.getColumnIndex(TbEvents.Event_ID)));
        setStart(cursor.getLong(cursor.getColumnIndex(TbEvents.Start)));
        setEnd(cursor.getLong(cursor.getColumnIndex(TbEvents.End)));

    }

    public RemoteEvent() {
    }

    @Override
    public LinkedHashMap<String, String> getIdentifiers() {

        LinkedHashMap<String, String> identifiers = super.getIdentifiers();
        identifiers.put(IDENTIFIERS[0], getEvent_id()+"");

        return identifiers;
    }

    @Override
    public void populateContentValues(ContentValues values) {
        values.put(TbEvents.CreateDate, getCreatedAt());
        values.put(TbEvents.UpdateDate, getUpdatedAt());
        values.put(TbEvents.SYNC_STATUS, getSyncStatus() != null ? getSyncStatus().ordinal() : null);
        values.put(TbEvents.IS_DELETED, getIsDeleted());

        values.put(TbEvents.Event_ID, getEvent_id());
        values.put(TbEvents.Start, getStart());
        values.put(TbEvents.End, getEnd());
    }
}
