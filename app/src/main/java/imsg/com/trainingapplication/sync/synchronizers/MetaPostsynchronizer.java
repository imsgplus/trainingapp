package imsg.com.trainingapplication.sync.synchronizers;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import imsg.com.trainingapplication.app.Application;
import imsg.com.trainingapplication.database.DatabaseHelper;
import imsg.com.trainingapplication.database.TbMetaPost;
import imsg.com.trainingapplication.providers.ImsgContentProvider;
import imsg.com.trainingapplication.sync.remote.RemoteMetaPost;
import imsg.com.trainingapplication.sync.remote.RemoteObject;
import imsg.com.trainingapplication.utils.DateUtil;
import imsg.com.trainingapplication.utils.SyncUtil;

/**
 * Created by hinh1 on 9/8/2017.
 */

public class MetaPostsynchronizer extends BaseSynchronizer<RemoteMetaPost> {
    private static final String TAG = MetaPostsynchronizer.class.getSimpleName();

    private boolean mAllowDeletion;
    public MetaPostsynchronizer(Context context, boolean allowDeletion) {
        super(context);
        mAllowDeletion = allowDeletion;
    }


    @Override
    protected void performSynchronizationOperations(Context context, List<RemoteMetaPost> inserts, List<RemoteMetaPost> updates, List<Long> deletions) {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        if( inserts.size() > 0 ) {
            doInsertOptimised(inserts);
        }

        for (RemoteMetaPost w : updates) {
            SyncUtil.Selection selection = SyncUtil.buildSelection(w.getIdentifiers());
            ContentProviderOperation op = ContentProviderOperation
                    .newUpdate(ImsgContentProvider.URIS.MetaPost_URI)
                    .withSelection(selection.getQuery(), selection.getValues())
                    .withValues(getContentValuesForRemoteEntity(w)).build();

            operations.add(op);
        }

        if( mAllowDeletion ) {
            for (Long id : deletions) {
                ContentProviderOperation op = ContentProviderOperation
                        .newDelete(ImsgContentProvider.URIS.MetaPost_URI)
                        .withSelection(TbMetaPost.ID + " = ? AND (" + TbMetaPost.SYNC_STATUS + "=? OR " + TbMetaPost.IS_DELETED + "=?)",
                                new String[]{String.valueOf(id), RemoteObject.SyncStatus.NO_CHANGES.ordinal() + "", "1"})
                        .build();

                operations.add(op);
            }
        }


        try {
            if( inserts.size() > 0 || operations.size() > 0 ) {
                context.getContentResolver().applyBatch(ImsgContentProvider.AUTHORITY, operations);
                context.getContentResolver().notifyChange(ImsgContentProvider.URIS.MetaPost_URI, null);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean isRemoteEntityNewerThanLocal(RemoteMetaPost remote, Cursor c) {
        try {
            Calendar remoteUpdatedTime = DateUtil.convertToDate(remote.getUpdatedAt());
            Calendar localUpdatedTime = DateUtil.convertToDate(c.getString(c.getColumnIndex(TbMetaPost.UpdateDate)));

            if( remoteUpdatedTime == null || localUpdatedTime == null )
                return true;

            RemoteObject.SyncStatus localSyncStatus = RemoteObject.SyncStatus.getSyncStatusFromCode(c.getInt(c.getColumnIndex(TbMetaPost.SYNC_STATUS)));

            return (remoteUpdatedTime.getTimeInMillis() > localUpdatedTime.getTimeInMillis()) && !localSyncStatus.equals(RemoteObject.SyncStatus.QUEUED_TO_SYNC);


        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }


    public static List<Long> doInsertOptimised(List<RemoteMetaPost> inserts) {

        if( inserts == null )
            return null;

        Context context = Application.getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        DatabaseUtils.InsertHelper inserter = new DatabaseUtils.InsertHelper(db, TbMetaPost.TABLE_NAME);
        List<Long> insertedIds = new ArrayList<>();
        RemoteMetaPost remoteMetaPost;

        db.beginTransaction();

        try {
            int len = inserts.size();
            for (int i = 0; i < len; i++) {

                remoteMetaPost = inserts.get(i);

                inserter.prepareForInsert();

                if( remoteMetaPost.getCreatedAt() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.CreateDate), remoteMetaPost.getCreatedAt());

                if( remoteMetaPost.getUpdatedAt() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.UpdateDate), remoteMetaPost.getUpdatedAt());

                if( remoteMetaPost.getSyncStatus() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.SYNC_STATUS), remoteMetaPost.getSyncStatus().ordinal());

                if( remoteMetaPost.getIsDeleted() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.IS_DELETED), remoteMetaPost.getIsDeleted());

                if( remoteMetaPost.getPostId() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.PostId), remoteMetaPost.getPostId());

                if( remoteMetaPost.getMetaId() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.MetaPost_ID), remoteMetaPost.getMetaId());

                if( remoteMetaPost.getPost_Id() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.Post_Id), remoteMetaPost.getPost_Id());

                if( remoteMetaPost.getKey() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.Key), remoteMetaPost.getKey());

                if( remoteMetaPost.getValue() != null )
                    inserter.bind(inserter.getColumnIndex(TbMetaPost.Value), remoteMetaPost.getValue());




                insertedIds.add(inserter.execute());
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            inserter.close();
        }
        return insertedIds;
    }
}
