package imsg.com.trainingapplication.sync.synchronizers;


import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import imsg.com.trainingapplication.app.Application;
import imsg.com.trainingapplication.database.DatabaseHelper;
import imsg.com.trainingapplication.database.TbPosts;
import imsg.com.trainingapplication.providers.ImsgContentProvider;
import imsg.com.trainingapplication.sync.remote.RemoteObject;
import imsg.com.trainingapplication.sync.remote.RemotePosts;
import imsg.com.trainingapplication.utils.DateUtil;
import imsg.com.trainingapplication.utils.SyncUtil;

/**
 * Created by hinh1 on 9/8/2017.
 */

public class PostsSynchronizer extends BaseSynchronizer<RemotePosts> {
    private static final String TAG = PostsSynchronizer.class.getSimpleName();

    private boolean mAllowDeletion;
    public PostsSynchronizer(Context context, boolean allowDeletion) {
        super(context);
        mAllowDeletion = allowDeletion;
    }

    @Override
    protected void performSynchronizationOperations(Context context, List<RemotePosts> inserts, List<RemotePosts> updates, List<Long> deletions) {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        if( inserts.size() > 0 ) {
            doInsertOptimised(inserts);
        }

        for (RemotePosts w : updates) {
            SyncUtil.Selection selection = SyncUtil.buildSelection(w.getIdentifiers());
            ContentProviderOperation op = ContentProviderOperation
                    .newUpdate(ImsgContentProvider.URIS.Post_URI)
                    .withSelection(selection.getQuery(), selection.getValues())
                    .withValues(getContentValuesForRemoteEntity(w)).build();

            operations.add(op);
        }

        if( mAllowDeletion ) {
            for (Long id : deletions) {
                ContentProviderOperation op = ContentProviderOperation
                        .newDelete(ImsgContentProvider.URIS.Post_URI)
                        .withSelection(TbPosts.ID + " = ? AND (" + TbPosts.SYNC_STATUS + "=? OR " + TbPosts.IS_DELETED + "=?)",
                                new String[]{String.valueOf(id), RemoteObject.SyncStatus.NO_CHANGES.ordinal() + "", "1"})
                        .build();

                operations.add(op);
            }
        }


        try {
            if( inserts.size() > 0 || operations.size() > 0 ) {
                context.getContentResolver().applyBatch(ImsgContentProvider.AUTHORITY, operations);
                context.getContentResolver().notifyChange(ImsgContentProvider.URIS.Post_URI, null);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean isRemoteEntityNewerThanLocal(RemotePosts remote, Cursor c) {
        try {
            Calendar remoteUpdatedTime = DateUtil.convertToDate(remote.getUpdatedAt());
            Calendar localUpdatedTime = DateUtil.convertToDate(c.getString(c.getColumnIndex(TbPosts.UpdateDate)));

            if( remoteUpdatedTime == null || localUpdatedTime == null )
                return true;

            RemoteObject.SyncStatus localSyncStatus = RemoteObject.SyncStatus.getSyncStatusFromCode(c.getInt(c.getColumnIndex(TbPosts.SYNC_STATUS)));

            return (remoteUpdatedTime.getTimeInMillis() > localUpdatedTime.getTimeInMillis()) && !localSyncStatus.equals(RemoteObject.SyncStatus.QUEUED_TO_SYNC);


        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public static List<Long> doInsertOptimised(List<RemotePosts> inserts) {

        if( inserts == null )
            return null;

        Context context =Application.getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        DatabaseUtils.InsertHelper inserter = new DatabaseUtils.InsertHelper(db, TbPosts.TABLE_NAME);
        List<Long> insertedIds = new ArrayList<>();
        RemotePosts remotePosts;

        db.beginTransaction();

        try {
            int len = inserts.size();
            for (int i = 0; i < len; i++) {

                remotePosts = inserts.get(i);

                inserter.prepareForInsert();

                if( remotePosts.getCreatedAt() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.CreateDate), remotePosts.getCreatedAt());

                if( remotePosts.getUpdatedAt() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.UpdateDate), remotePosts.getUpdatedAt());

                if( remotePosts.getSyncStatus() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.SYNC_STATUS), remotePosts.getSyncStatus().ordinal());

                if( remotePosts.getIsDeleted() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.IS_DELETED), remotePosts.getIsDeleted());

                if( remotePosts.getPostId() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.Post_ID), remotePosts.getPostId());

                if( remotePosts.getPostAuthor() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.Post_Author), remotePosts.getPostAuthor());

                if( remotePosts.getPostDate() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.Post_Date), remotePosts.getPostDate());

                if( remotePosts.getPostContent() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.Post_Content), remotePosts.getPostContent());

                if( remotePosts.getPostTitle() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.Post_Title), remotePosts.getPostTitle());

                if( remotePosts.getPostName() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.Post_Name), remotePosts.getPostName());

                if( remotePosts.getGuid() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.Guid), remotePosts.getGuid());

                if( remotePosts.getPostType() != null )
                    inserter.bind(inserter.getColumnIndex(TbPosts.Post_Type), remotePosts.getPostType());


                insertedIds.add(inserter.execute());
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            inserter.close();
        }
        return insertedIds;
    }
}
