package imsg.com.trainingapplication.sync.synchronizers;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import imsg.com.trainingapplication.app.Application;
import imsg.com.trainingapplication.database.DatabaseHelper;
import imsg.com.trainingapplication.database.TbEvents;
import imsg.com.trainingapplication.providers.ImsgContentProvider;
import imsg.com.trainingapplication.sync.remote.RemoteEvent;
import imsg.com.trainingapplication.sync.remote.RemoteObject;
import imsg.com.trainingapplication.utils.DateUtil;
import imsg.com.trainingapplication.utils.SyncUtil;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class EventsSynchronizer extends BaseSynchronizer<RemoteEvent> {
    private static final String TAG = EventsSynchronizer.class.getSimpleName();

    private boolean mAllowDeletion;
    public EventsSynchronizer(Context context, boolean allowDeletion) {
        super(context);
        mAllowDeletion = allowDeletion;
    }

    @Override
    protected void performSynchronizationOperations(Context context, List<RemoteEvent> inserts, List<RemoteEvent> updates, List<Long> deletions) {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        if( inserts.size() > 0 ) {
            doInsertOptimised(inserts);
        }

        for (RemoteEvent w : updates) {
            SyncUtil.Selection selection = SyncUtil.buildSelection(w.getIdentifiers());
            ContentProviderOperation op = ContentProviderOperation
                    .newUpdate(ImsgContentProvider.URIS.Events_URI)
                    .withSelection(selection.getQuery(), selection.getValues())
                    .withValues(getContentValuesForRemoteEntity(w)).build();

            operations.add(op);
        }

        if( mAllowDeletion ) {
            for (Long id : deletions) {
                ContentProviderOperation op = ContentProviderOperation
                        .newDelete(ImsgContentProvider.URIS.Events_URI)
                        .withSelection(TbEvents.ID + " = ? AND (" + TbEvents.SYNC_STATUS + "=? OR " + TbEvents.IS_DELETED + "=?)",
                                new String[]{String.valueOf(id), RemoteObject.SyncStatus.NO_CHANGES.ordinal() + "", "1"})
                        .build();

                operations.add(op);
            }
        }


        try {
            if( inserts.size() > 0 || operations.size() > 0 ) {
                context.getContentResolver().applyBatch(ImsgContentProvider.AUTHORITY, operations);
                context.getContentResolver().notifyChange(ImsgContentProvider.URIS.Events_URI, null);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean isRemoteEntityNewerThanLocal(RemoteEvent remote, Cursor c) {
        try {
            Calendar remoteUpdatedTime = DateUtil.convertToDate(remote.getUpdatedAt());
            Calendar localUpdatedTime = DateUtil.convertToDate(c.getString(c.getColumnIndex(TbEvents.UpdateDate)));

            if( remoteUpdatedTime == null || localUpdatedTime == null )
                return true;

            RemoteObject.SyncStatus localSyncStatus = RemoteObject.SyncStatus.getSyncStatusFromCode(c.getInt(c.getColumnIndex(TbEvents.SYNC_STATUS)));

            return (remoteUpdatedTime.getTimeInMillis() > localUpdatedTime.getTimeInMillis()) && !localSyncStatus.equals(RemoteObject.SyncStatus.QUEUED_TO_SYNC);


        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public static List<Long> doInsertOptimised(List<RemoteEvent> inserts) {

        if( inserts == null )
            return null;

        Context context = Application.getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        DatabaseUtils.InsertHelper inserter = new DatabaseUtils.InsertHelper(db, TbEvents.TABLE_NAME);
        List<Long> insertedIds = new ArrayList<>();
        RemoteEvent remoteEvent;

        db.beginTransaction();

        try {
            int len = inserts.size();
            for (int i = 0; i < len; i++) {

                remoteEvent = inserts.get(i);

                inserter.prepareForInsert();

                if( remoteEvent.getCreatedAt() != null )
                    inserter.bind(inserter.getColumnIndex(TbEvents.CreateDate), remoteEvent.getCreatedAt());

                if( remoteEvent.getUpdatedAt() != null )
                    inserter.bind(inserter.getColumnIndex(TbEvents.UpdateDate), remoteEvent.getUpdatedAt());

                if( remoteEvent.getSyncStatus() != null )
                    inserter.bind(inserter.getColumnIndex(TbEvents.SYNC_STATUS), remoteEvent.getSyncStatus().ordinal());

                if( remoteEvent.getIsDeleted() != null )
                    inserter.bind(inserter.getColumnIndex(TbEvents.IS_DELETED), remoteEvent.getIsDeleted());

                if( remoteEvent.getEvent_id() != null )
                    inserter.bind(inserter.getColumnIndex(TbEvents.Event_ID), remoteEvent.getEvent_id());

                if( remoteEvent.getStart() != null )
                    inserter.bind(inserter.getColumnIndex(TbEvents.Start), remoteEvent.getStart());

                if( remoteEvent.getEnd() != null )
                    inserter.bind(inserter.getColumnIndex(TbEvents.End), remoteEvent.getEnd());


                insertedIds.add(inserter.execute());
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            inserter.close();
        }
        return insertedIds;
    }
}
