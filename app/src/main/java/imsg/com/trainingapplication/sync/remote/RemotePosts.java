
package imsg.com.trainingapplication.sync.remote;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;

import imsg.com.trainingapplication.database.TbPosts;

public class RemotePosts extends RemoteObject{

    public static final String[] IDENTIFIERS = new String[] {
            TbPosts.Post_ID
    };

    @SerializedName("id")
    @Expose
    private Long postId;
    @SerializedName("post_author")
    @Expose
    private Long postAuthor;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("post_content")
    @Expose
    private String postContent;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_name")
    @Expose
    private String postName;
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("post_type")
    @Expose
    private String postType;

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(Long postAuthor) {
        this.postAuthor = postAuthor;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public RemotePosts(final Cursor cursor) {

        setId(cursor.getLong(cursor.getColumnIndex(TbPosts.ID)));
        setCreatedAt(cursor.getString(cursor.getColumnIndex(TbPosts.CreateDate)));
        setUpdatedAt(cursor.getString(cursor.getColumnIndex(TbPosts.UpdateDate)));
        setSyncStatus(SyncStatus.getSyncStatusFromCode(cursor.getInt(cursor.getColumnIndex(TbPosts.SYNC_STATUS))));
        setIsDeleted(cursor.getInt(cursor.getColumnIndex(TbPosts.IS_DELETED)) == 1);

        setPostId(cursor.getLong(cursor.getColumnIndex(TbPosts.Post_ID)));
        setPostAuthor(cursor.getLong(cursor.getColumnIndex(TbPosts.Post_Author)));
        setPostDate(cursor.getString(cursor.getColumnIndex(TbPosts.Post_Date)));
        setPostContent(cursor.getString(cursor.getColumnIndex(TbPosts.Post_Content)));
        setPostTitle(cursor.getString(cursor.getColumnIndex(TbPosts.Post_Title)));
        setPostName(cursor.getString(cursor.getColumnIndex(TbPosts.Post_Name)));
        setGuid(cursor.getString(cursor.getColumnIndex(TbPosts.Guid)));
        setPostType(cursor.getString(cursor.getColumnIndex(TbPosts.Post_Type)));

    }

    public RemotePosts() {
    }

    @Override
    public LinkedHashMap<String, String> getIdentifiers() {

        LinkedHashMap<String, String> identifiers = super.getIdentifiers();
        identifiers.put(IDENTIFIERS[0], getPostId()+"");

        return identifiers;
    }


    @Override
    public void populateContentValues(ContentValues values) {
        values.put(TbPosts.CreateDate, getCreatedAt());
        values.put(TbPosts.UpdateDate, getUpdatedAt());
        values.put(TbPosts.SYNC_STATUS, getSyncStatus() != null ? getSyncStatus().ordinal() : null);
        values.put(TbPosts.IS_DELETED, getIsDeleted());

        values.put(TbPosts.Post_ID, getPostId());
        values.put(TbPosts.Post_Author, getPostAuthor());
        values.put(TbPosts.Post_Date, getPostDate());
        values.put(TbPosts.Post_Content, getPostContent());
        values.put(TbPosts.Post_Title, getPostTitle());
        values.put(TbPosts.Post_Name, getPostName());
        values.put(TbPosts.Guid, getGuid());
        values.put(TbPosts.Post_Type, getPostType());
    }
}
