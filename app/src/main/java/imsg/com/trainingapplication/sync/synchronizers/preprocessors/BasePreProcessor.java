package imsg.com.trainingapplication.sync.synchronizers.preprocessors;


import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemoteObject;

/**
 * Created by Michael on 2014-03-11.
 */
public abstract class BasePreProcessor<T extends RemoteObject> {

    public abstract void preProcessRemoteRecords(List<T> records);

}