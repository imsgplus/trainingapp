package imsg.com.trainingapplication.sync.remote;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;

import imsg.com.trainingapplication.database.TbTerms;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class RemoteTerm extends RemoteObject{

    public static final String[] IDENTIFIERS = new String[] {
            TbTerms.Term_ID
    };

    @SerializedName("id")
    @Expose
    private Long term_id;
    @SerializedName("term_name")
    @Expose
    private String termName;
    @SerializedName("slug")
    @Expose
    private String slug;

    public Long getTerm_id() {
        return term_id;
    }

    public void setTerm_id(Long term_id) {
        this.term_id = term_id;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public RemoteTerm(final Cursor cursor) {

        setId(cursor.getLong(cursor.getColumnIndex(TbTerms.ID)));
        setCreatedAt(cursor.getString(cursor.getColumnIndex(TbTerms.CreateDate)));
        setUpdatedAt(cursor.getString(cursor.getColumnIndex(TbTerms.UpdateDate)));
        setSyncStatus(SyncStatus.getSyncStatusFromCode(cursor.getInt(cursor.getColumnIndex(TbTerms.SYNC_STATUS))));
        setIsDeleted(cursor.getInt(cursor.getColumnIndex(TbTerms.IS_DELETED)) == 1);

        setSlug(cursor.getString(cursor.getColumnIndex(TbTerms.Slug)));
        setTerm_id(cursor.getLong(cursor.getColumnIndex(TbTerms.Term_ID)));
        setTermName(cursor.getString(cursor.getColumnIndex(TbTerms.Name)));

    }

    public RemoteTerm() {
    }

    @Override
    public LinkedHashMap<String, String> getIdentifiers() {

        LinkedHashMap<String, String> identifiers = super.getIdentifiers();
        identifiers.put(IDENTIFIERS[0], getTerm_id()+"");

        return identifiers;
    }

    @Override
    public void populateContentValues(ContentValues values) {
        values.put(TbTerms.CreateDate, getCreatedAt());
        values.put(TbTerms.UpdateDate, getUpdatedAt());
        values.put(TbTerms.SYNC_STATUS, getSyncStatus() != null ? getSyncStatus().ordinal() : null);
        values.put(TbTerms.IS_DELETED, getIsDeleted());

        values.put(TbTerms.Term_ID, getTerm_id());
        values.put(TbTerms.Name, getTermName());
        values.put(TbTerms.Slug, getSlug());
    }
}
