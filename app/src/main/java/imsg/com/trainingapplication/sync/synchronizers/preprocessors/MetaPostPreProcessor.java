package imsg.com.trainingapplication.sync.synchronizers.preprocessors;

import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemoteMetaPost;
import imsg.com.trainingapplication.sync.remote.RemoteObject;

/**
 * Created by hinh1 on 9/8/2017.
 */

public class MetaPostPreProcessor extends BasePreProcessor<RemoteMetaPost> {
    @Override
    public void preProcessRemoteRecords(List<RemoteMetaPost> records) {
        for( RemoteMetaPost metaPost : records ) {
            if( metaPost != null ) {
                metaPost.setIsDeleted(false);
                metaPost.setSyncStatus(RemoteObject.SyncStatus.NO_CHANGES);
            }
        }
    }
}
