package imsg.com.trainingapplication.sync.remote;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedHashMap;

/**
 * 
 */
public abstract class RemoteObject implements Parcelable {

    public static enum SyncStatus
    {
        NO_CHANGES, QUEUED_TO_SYNC, TEMP;

        public static SyncStatus getSyncStatusFromCode(Integer code) {
            if (code == null || code < 0 || code > values().length - 1) {
                return NO_CHANGES;
            }

            return values()[code];
        }
    }


    private Long mId;

    private String createDate;
    
    private String updateDate;

    private SyncStatus mSyncStatus;
    private Boolean mIsDeleted;

    public RemoteObject() {}


    protected RemoteObject(Long id, String createdAt, String updatedAt, SyncStatus syncStatus, Boolean isDeleted) {
        mId = id;
        createDate = createdAt;
        updateDate = updatedAt;
        mSyncStatus = syncStatus;
        mIsDeleted = isDeleted;
    }



    public RemoteObject(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(this.getId() != null ? 1:0);

        if( this.getId() != null )
            dest.writeLong(this.getId());



        dest.writeInt(this.getCreatedAt() != null ? 1:0);

        if( this.getCreatedAt() != null )
            dest.writeString(this.getCreatedAt());



        dest.writeInt(this.getUpdatedAt() != null ? 1:0);

        if( this.getUpdatedAt() != null )
            dest.writeString(this.getUpdatedAt());


        dest.writeInt(this.getSyncStatus() != null ? 1:0);

        if( this.getSyncStatus() != null )
            dest.writeInt(this.getSyncStatus().ordinal());


        dest.writeInt(this.getIsDeleted() != null ? 1:0);

        if( this.getIsDeleted() != null )
            dest.writeInt(this.getIsDeleted() ? 1:0);

    }


    public void readFromParcel(Parcel in) {

        if( in.readInt() == 1 )
            setId(in.readLong());

        if( in.readInt() == 1 )
            setCreatedAt(in.readString());

        if( in.readInt() == 1 )
            setUpdatedAt(in.readString());

        if( in.readInt() == 1 )
            setSyncStatus(SyncStatus.getSyncStatusFromCode(in.readInt()));

        if( in.readInt() == 1 )
            setIsDeleted(in.readInt() == 1);
    }



    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getCreatedAt() {
        return createDate;
    }

    public void setCreatedAt(String createdAt) {
        createDate = createdAt;
    }

    public String getUpdatedAt() {
        return updateDate;
    }

    public void setUpdatedAt(String updatedAt) {
        updateDate = updatedAt;
    }

    public SyncStatus getSyncStatus() {
        return mSyncStatus;
    }

    public void setSyncStatus(SyncStatus syncStatus) {
        mSyncStatus = syncStatus;
    }

    public Boolean getIsDeleted() {
        return mIsDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        mIsDeleted = isDeleted;
    }

    public LinkedHashMap<String,String> getIdentifiers()
    {
        LinkedHashMap<String, String> identifiers = new LinkedHashMap<>();
        return identifiers;
    }

    public abstract void populateContentValues(ContentValues values);
}

