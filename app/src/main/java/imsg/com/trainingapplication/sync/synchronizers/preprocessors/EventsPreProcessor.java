package imsg.com.trainingapplication.sync.synchronizers.preprocessors;

import java.util.List;

import imsg.com.trainingapplication.sync.remote.RemoteEvent;
import imsg.com.trainingapplication.sync.remote.RemoteObject;

/**
 * Created by hinh1 on 10/19/2017.
 */

public class EventsPreProcessor extends BasePreProcessor<RemoteEvent> {
    @Override
    public void preProcessRemoteRecords(List<RemoteEvent> records) {
        for( RemoteEvent posts : records ) {
            if( posts != null ) {
                posts.setIsDeleted(false);
                posts.setSyncStatus(RemoteObject.SyncStatus.NO_CHANGES);
            }
        }
    }
}
