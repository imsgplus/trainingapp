package imsg.com.trainingapplication.app;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imsg.com.trainingapplication.network.APIServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Dung on 7/13/2017.
 */

public class Application extends MultiDexApplication {
    public static Retrofit mRetrofit;
    private static Context sContext;
    private static Gson gson;
    public static final String BASE_API = " http://8b587e9f.ngrok.io/webserver/";

    public static APIServer APISever;
    @Override
    public void onCreate() {
        super.onCreate();

        sContext=getApplicationContext();
        gson = new GsonBuilder()
                .setLenient()
                .create();


        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_API)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        APISever  = mRetrofit.create(APIServer.class);
    }
    public static final Context getContext() {
        return sContext;
    }
}
